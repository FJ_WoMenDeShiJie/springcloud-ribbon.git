package com.ribbon.serviceribbon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * /**
 *
 * @author yujiang
 * @description
 * @date 2019/7/22 13:27
 */
@RestController
public class SeeParamController {


    @Autowired
    SeeParamService seeParamService;

    @RequestMapping(value = "/seeParam")
    public String see(@RequestParam String param) {
        return seeParamService.seeService(param) + " , 我来自服务：ribbon 。 ";
    }
}
