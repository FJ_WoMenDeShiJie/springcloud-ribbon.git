package com.ribbon.serviceribbon;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author yujiang
 * @description
 * @date 2019/7/22 11:45
 */
@Service
public class SeeParamService {

    @Autowired
    RestTemplate restTemplate;

    /**
     * 注解 @HystrixCommand ：
     * 开启熔断器，指定熔断时回调方法为 seeParamError
     *
     * @param param
     * @return
     */
    @HystrixCommand(fallbackMethod = "seeParamError")
    public String seeService(String param) {
        return restTemplate.getForObject("http://see-param/seeParam?param=" + param, String.class);
    }

    /**
     * 熔断 调用此方法
     *
     * @return
     */
    public String seeParamError(String param) {
        return " 哎，别瞅了，别瞅了，服务已经 go die ...";
    }

}
